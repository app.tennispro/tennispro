package com.borakay.tennispro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.firebase.client.Firebase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import com.otaliastudios.cameraview.CameraView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    View view;
    CameraView camera_view;
    boolean isDetected= false;
    Button btn_start_again;
    private static final String TAG = "MainActivity";
    DatabaseReference databaseReference;
    FirebaseVisionBarcodeDetectorOptions options;
    FirebaseVisionBarcodeDetector detector;
    Button btnrepair;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnrepair=(Button)findViewById(R.id.btnrepair);

        btnrepair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i=new Intent(MainActivity.this,QrDataActivity.class);
                startActivity(i);
            }
        });


    }
}
