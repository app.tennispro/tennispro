package com.borakay.tennispro;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomerDetailActivity extends AppCompatActivity {


    private static final String TAG = "CustomerDetail";

    private ArrayList<CustomerDetailModel> mExampleList;

    private FirebaseFirestore firestore=FirebaseFirestore.getInstance();
    CollectionReference dbcustomer=firestore.collection("t_customer_master");
    private CustomerAdapter customerAdapter;
    RadioButton rdmr,rdmrs;
    TextInputEditText ednom,edprenom,edemail,edmobile,edfamilynom,edfamilyprenom,edfamilyemail;
   Spinner spvotreclub,spAge,spfamilylien;
   Button btnnext,btnfamilymember;
   SearchView simpleSearchView;
    Intent intent;
    String value,familynom,familyprenom,familyemail,relation1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_detail);
        value =getIntent().getStringExtra("value");

        init();
        //
        setRecyclerview();

    }




    private void init() {
        //EditText
        
        ednom=(TextInputEditText)findViewById(R.id.etNOM);
        edprenom=(TextInputEditText)findViewById(R.id.etprenom);
        edemail=(TextInputEditText)findViewById(R.id.etemail);
        edmobile=(TextInputEditText)findViewById(R.id.etmobile);

        simpleSearchView=(SearchView) findViewById(R.id.simpleSearchView);
     simpleSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
         @Override
         public boolean onQueryTextSubmit(String query) {
             return false;
         }

         @Override
         public boolean onQueryTextChange(String newText) {
             customerAdapter.getFilter().filter(newText);
             return true;
         }
     });





        //Radio Button
        rdmr=(RadioButton)findViewById(R.id.rdsir);
        rdmrs=(RadioButton)findViewById(R.id.rdmrs);

        //Button for family member


        btnfamilymember=(Button)findViewById(R.id.btnfamilymember);
        btnfamilymember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CustomerDetailActivity.this);
               final AlertDialog OptionDialog = builder.create();
                LayoutInflater inflater=CustomerDetailActivity.this.getLayoutInflater();
                //this is what I did to added the layout to the alert dialog
                final View layout=inflater.inflate(R.layout.bottom_family_sheet,null);
                OptionDialog.setView(layout);
                edfamilynom=(TextInputEditText) layout.findViewById(R.id.etfamilyNOM);
               edfamilyprenom=(TextInputEditText)layout.findViewById(R.id.etfamilyprenom);

               final TextInputEditText edfamilyemail=(TextInputEditText)layout.findViewById(R.id.etfamilyemail);
               spfamilylien=(Spinner)layout.findViewById(R.id.sprelation);

                final List<String> relation=new ArrayList<>();
                relation.add("File");
                relation.add("Fille");
                relation.add("Pere");
                relation.add("Mere");
                relation.add("Frere");
                final ArrayAdapter<String> arrayAdapter;
                arrayAdapter=new ArrayAdapter<String>(CustomerDetailActivity.this,android.R.layout.simple_spinner_item,relation);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spfamilylien.setAdapter(arrayAdapter);

               final Button btnsave=(Button)layout.findViewById(R.id.btnsave);
               btnsave.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {

                     familynom=edfamilynom.getText().toString();
                       familyprenom=edfamilyprenom.getText().toString();
                        familyemail=edfamilyemail.getText().toString();
                        relation1 =spfamilylien.getSelectedItem().toString();

                       edfamilynom.addTextChangedListener(new TextWatcher() {
                           @Override
                           public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                           }
                           Boolean isValid=true;
                           @Override
                           public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if (edfamilynom.getText().toString().isEmpty()){
                                edfamilynom.setError("Enter your last name");
                                isValid=false;
                            }else{

                                isValid=true;
                            }
                           }

                           @Override
                           public void afterTextChanged(Editable s) {

                           }
                       });

                       edfamilyprenom.addTextChangedListener(new TextWatcher() {
                           @Override
                           public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                           }
                           Boolean IsValid=true;

                           @Override
                           public void onTextChanged(CharSequence s, int start, int before, int count) {

                               if (edfamilyprenom.getText().toString().isEmpty()){
                                   edfamilyprenom.setError("Enter your First name");
                                   IsValid=false;
                               }else{

                                   IsValid=true;
                               }

                           }

                           @Override
                           public void afterTextChanged(Editable s) {

                           }
                       });

                       edfamilyemail.addTextChangedListener(new TextWatcher() {
                           @Override
                           public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                           }
                            Boolean IsValid=true;
                           @Override
                           public void onTextChanged(CharSequence s, int start, int before, int count) {
                               if (edfamilyemail.getText().toString().isEmpty()){
                                   edfamilyemail.setError("Enter your email Id");
                                   IsValid=false;
                               }else{

                                   IsValid=true;
                               }

                           }

                           @Override
                           public void afterTextChanged(Editable s) {

                           }
                       });
                       OptionDialog.dismiss();
                   }
               });

                OptionDialog.show();


            }

        });



        //Spinner
        spAge = (Spinner) findViewById(R.id.spage);
        final List<String> age=new ArrayList<>();
        age.add("40 Ans");
        age.add("45 Ans");
        final ArrayAdapter<String> arrayAdapter;
        arrayAdapter=new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,age);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spAge.setAdapter(arrayAdapter);


        spvotreclub=(Spinner)findViewById(R.id.spinner);
        final List<String> votreclub=new ArrayList<>();
        votreclub.add("TCBB");
        votreclub.add("TCP");
        votreclub.add("TC16");
        final ArrayAdapter<String> adapter;
        adapter=new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,votreclub);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spvotreclub.setAdapter(adapter);



        //Button for next
        btnnext=(Button)findViewById(R.id.btnnext);

        //Onclick method
        btnnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String nom=ednom.getText().toString();
                String prenom=edprenom.getText().toString();
                String email=edemail.getText().toString();
                String mobile=edmobile.getText().toString();
                String age=spAge.getSelectedItem().toString();
                String club=spvotreclub.getSelectedItem().toString();


                Intent i=new Intent(CustomerDetailActivity.this,RacketDetail.class);
                i.putExtra("value",value);
                i.putExtra("nom",nom);
                i.putExtra("prenom",prenom);
                i.putExtra("email",email);
                i.putExtra("mobile",mobile);
                i.putExtra("age",age);
                i.putExtra("club",club);
                i.putExtra("familynom",familynom);
                i.putExtra("familyprenom",familyprenom);
                i.putExtra("familyemail",familyemail);
                i.putExtra("relation",relation1);
                startActivity(i);


            }
        });


    }



    private void setRecyclerview() {
        Query query = dbcustomer.orderBy("nom", Query.Direction.DESCENDING);

        FirestoreRecyclerOptions<CustomerDetailModel> options = new FirestoreRecyclerOptions.Builder<CustomerDetailModel>()
                .setQuery(query, CustomerDetailModel.class)
                .build();

        customerAdapter = new CustomerAdapter(options);
        RecyclerView recyclerView = findViewById(R.id.rvsearchlist);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(customerAdapter);
    }
    @Override
    protected void onStart() {
        super.onStart();
        customerAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        customerAdapter.stopListening();
    }


}
