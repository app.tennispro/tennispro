package com.borakay.tennispro;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.TextView;

public class DashboardActivity extends AppCompatActivity {

TextView tvqrcode,tvemail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
      //  setToolbar();
        init();

    }

    private void init() {
        tvqrcode=(TextView)findViewById(R.id.tvqrcode);
        tvemail=(TextView)findViewById(R.id.tvracketcordee);
        tvqrcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(DashboardActivity.this,QrcodeActivity.class);
                startActivity(i);

            }
        });


    }



    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        getMenuInflater().inflate(R.menu.menubar, m);
        return true;
    }
}
