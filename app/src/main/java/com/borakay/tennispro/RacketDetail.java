package com.borakay.tennispro;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import com.firebase.client.annotations.Nullable;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.auth.User;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class RacketDetail extends AppCompatActivity {

    Spinner spbrand, spgamme, spmodele, sptailledemanche, sppoids, spetat, spannee;
    SearchView searchView;
    RadioButton rdoui, rdnon, rdnsp;
    private FirebaseFirestore racketstore = FirebaseFirestore.getInstance();
    CollectionReference dbmarque_model = racketstore.collection("t_racket");
    Button btnsuivant;
    ArrayList<Racketmodel> listofracket;
    ImageView captureimage;
    private static final int CAMERA_PIC_REQUEST = 22;
    public static int count = 0;
    String value, nom, prenom, email, mobile, club, age, familynom, familyprenom, familyemail, relation1, marque, gamme1, model, poids, tailledemanche, annee, etat, acheteenmagasin;
    Bitmap photo;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_racket_detail);

        //transfer the data on activity two another
        value = getIntent().getStringExtra("value");
        nom = getIntent().getStringExtra("nom");
        prenom = getIntent().getStringExtra("prenom");
        email = getIntent().getStringExtra("email");
        mobile = getIntent().getStringExtra("mobile");
        age = getIntent().getStringExtra("age");
        club = getIntent().getStringExtra("club");
        familynom = getIntent().getStringExtra("familynom");
        familyprenom = getIntent().getStringExtra("familyprenom");
        familyemail = getIntent().getStringExtra("familyemail");
        relation1 = getIntent().getStringExtra("relation");

        init();
        addSpinnerdata();
        addsearchview();


    }

    private void addsearchview() {
        getUser();

        searchView = (SearchView) findViewById(R.id.searchforracquet);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Searchracquet(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Searchracquet(s);

                return false;
            }
        });

    }

    private void getUser() {
        racketstore.collection("t_racket_marque_model_master").whereEqualTo("marque", "Wilson")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {//.whereEqualTo("marque", "W")
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            System.err.println("Listen failed:" + e);
                            return;
                        }
                        listofracket = new ArrayList<Racketmodel>();

                        //List<Racketmodel> doc2 = new ArrayList<>();
                        for (DocumentSnapshot doc : snapshots) {
                            Racketmodel racketmodel = new Racketmodel();
                            racketmodel = doc.toObject(Racketmodel.class);
                            //doc2 = Collections.singletonList(doc.toObject(Racketmodel.class));
                            listofracket.add(racketmodel);
                        }

                    }
                });


    }


    private void Searchracquet(String s) {
        if (s.length() > 0)
            s = s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();

        ArrayList<Racketmodel> results = new ArrayList<>();
        for (Racketmodel rac : listofracket) {
            if (rac.getMarque() != null && rac.getMarque().contains(s)) {
                results.add(rac);
            }
        }

    }


    private void addSpinnerdata() {

        //Racket Marque List

        List<String> racketmodels = new ArrayList<>();
        racketmodels.add(" ");
        racketmodels.add("Babolat");
        racketmodels.add("Dunlop");
        racketmodels.add("Head");
        racketmodels.add("Prince");
        racketmodels.add("ProKennex");
        racketmodels.add("TecniFibre");
        racketmodels.add("TennisPro");
        racketmodels.add("ToalSon");
        racketmodels.add("Volkl");
        racketmodels.add("Wilson");
        racketmodels.add("Yonnex");
        racketmodels.add("Racquettes Occasion");
        racketmodels.add("Racquettes Test");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, racketmodels);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spbrand.setAdapter(dataAdapter);
        List<String> gamme = new ArrayList<>();
        gamme.add(" ");
        gamme.add("Pure Strike");
        gamme.add("Pure Drive");


        ArrayAdapter<String> gammes = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, gamme);

        // Drop down layout style - list view with radio button
        gammes.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spgamme.setAdapter(gammes);

        List<String> model = new ArrayList<>();
        model.add(" ");
        model.add("Pure Strike Tour");
        model.add("Pure Strike 100");


        ArrayAdapter<String> models = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, model);

        // Drop down layout style - list view with radio button
        models.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spmodele.setAdapter(models);
        List<String> poids = new ArrayList<>();
        poids.add("");
        poids.add("320");
        poids.add("305");


        ArrayAdapter<String> poid = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, poids);

        // Drop down layout style - list view with radio button
        poid.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        sppoids.setAdapter(poid);


        List<String> tailledemanche = new ArrayList<>();
        tailledemanche.add("");
        tailledemanche.add("L0(4)");
        tailledemanche.add("L1(4 1/8)");
        tailledemanche.add("L2(4 1/4)");
        tailledemanche.add("L3(4 3/8)");
        tailledemanche.add("L4(4 1/2)");

        ArrayAdapter<String> tailedemanche = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tailledemanche);

        // Drop down layout style - list view with radio button
        tailedemanche.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        sptailledemanche.setAdapter(tailedemanche);


        //Spinner for year
        List<String> annee = new ArrayList<>();
        annee.add("");
        annee.add("2019");
        annee.add("2018");
        annee.add("2017");
        annee.add("2016");
        annee.add("2015");

        ArrayAdapter<String> year = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, annee);

        // Drop down layout style - list view with radio button
        year.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spannee.setAdapter(year);

        //Spinner for etat

        List<String> etat = new ArrayList<>();
        etat.add("");
        etat.add("Neuve");
        etat.add("Bon etat");
        etat.add("Abimee");
        etat.add("A Remlacer");

        ArrayAdapter<String> newra = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, etat);

        // Drop down layout style - list view with radio button
        newra.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spetat.setAdapter(newra);


    }

    private void init() {

        //ImageView
        final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/picFolder/";
        File newdir = new File(dir);
        newdir.mkdirs();
        captureimage = (ImageView) findViewById(R.id.ivcaptureimag);
        captureimage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Couldn't load photo", Toast.LENGTH_LONG).show();
                }
            }
        });

        //Button
        btnsuivant = (Button) findViewById(R.id.btnsuivant);
        btnsuivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                marque = spbrand.getSelectedItem().toString();
                gamme1 = spgamme.getSelectedItem().toString();
                model = spmodele.getSelectedItem().toString();
                poids = sppoids.getSelectedItem().toString();
                tailledemanche = sptailledemanche.getSelectedItem().toString();
                annee = spannee.getSelectedItem().toString();
                etat = spetat.getSelectedItem().toString();


                intent = new Intent(RacketDetail.this, StringDetailActivity.class);

                intent.putExtra("value", value);
                intent.putExtra("nom", nom);
                intent.putExtra("prenom", prenom);
                intent.putExtra("email", email);
                intent.putExtra("mobile", mobile);
                intent.putExtra("club", club);
                intent.putExtra("age", age);
                intent.putExtra("familynom", familynom);
                intent.putExtra("familyprenom", familyprenom);
                intent.putExtra("familyemail", familyemail);
                intent.putExtra("relation", relation1);
                intent.putExtra("marque", marque);
                intent.putExtra("gamme1", gamme1);
                intent.putExtra("model", model);
                intent.putExtra("poids", poids);
                intent.putExtra("tailledemanche", tailledemanche);
                intent.putExtra("annee", annee);
                intent.putExtra("etat", etat);
                intent.putExtra("radioChosen", acheteenmagasin);
                intent.putExtra("image", photo);
                startActivity(intent);


            }
        });


        //Spinner
        spbrand = (Spinner) findViewById(R.id.spmarque);
        spgamme = (Spinner) findViewById(R.id.spgamme);
        spmodele = (Spinner) findViewById(R.id.spmodele);
        sptailledemanche = (Spinner) findViewById(R.id.spsleevesize);
        sppoids = (Spinner) findViewById(R.id.spPoids);
        spetat = (Spinner) findViewById(R.id.spetat);
        spannee = (Spinner) findViewById(R.id.spanne);

        //RadioButton




        }

        @Override
        public void onActivityResult ( final int requestCode, int resultCode, Intent data){
            super.onActivityResult(requestCode, resultCode, data);
            try {
                switch (requestCode) {
                    case CAMERA_PIC_REQUEST:
                        if (resultCode == RESULT_OK) {
                            try {
                                photo = (Bitmap) data.getExtras().get("data");

                                captureimage.setImageBitmap(photo);

                            } catch (Exception e) {
                                Toast.makeText(this, "Couldn't load photo", Toast.LENGTH_LONG).show();
                            }
                        }
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
            }
        }


    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()) {
            case R.id.rdyes:
            if (checked)
                acheteenmagasin = "radio button 1";
            break;
            case R.id.rdno:
                if (checked) acheteenmagasin = "radio button 2";
                break;
            case R.id.rdnsp:
                if (checked) acheteenmagasin = "radio button 3";
                break;
        }
    }
}




