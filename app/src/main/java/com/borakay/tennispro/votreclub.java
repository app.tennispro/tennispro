package com.borakay.tennispro;

public class votreclub {
    String clubname,active_status,active_date,Inactive_Date;

    public votreclub(){

    }

    public votreclub(String clubname, String active_status, String active_date, String inactive_Date) {
        this.clubname = clubname;
        this.active_status = active_status;
        this.active_date = active_date;
        Inactive_Date = inactive_Date;
    }

    public String getClubname() {
        return clubname;
    }

    public String getActive_status() {
        return active_status;
    }

    public String getActive_date() {
        return active_date;
    }

    public String getInactive_Date() {
        return Inactive_Date;
    }
}
