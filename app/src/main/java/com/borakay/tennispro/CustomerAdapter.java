package com.borakay.tennispro;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CustomerAdapter extends FirestoreRecyclerAdapter<CustomerDetailModel, CustomerAdapter.CustomerHolder> implements Filterable {

    private List<CustomerDetailModel> userModelList;
    private List<CustomerDetailModel> getUserModelListFiltered;
   // private FirestoreRecyclerOptions<CustomerDetailModel> getUserModelListFiltered;



    public CustomerAdapter(FirestoreRecyclerOptions<CustomerDetailModel> options) {
        super(options);

    }

    public CustomerAdapter(FirestoreRecyclerOptions<CustomerDetailModel> options, String text) {

        super(options);
    }

    class CustomerHolder extends RecyclerView.ViewHolder{
        TextView tvNom;
        TextView tvpreno;

        public CustomerHolder(View itemView) {
            super(itemView);
            tvNom=itemView.findViewById(R.id.tv_nom);
            tvpreno=itemView.findViewById(R.id.tv_Prenom);

        }
    }
    @Override
    protected void onBindViewHolder(@NonNull CustomerHolder holder, int position, @NonNull CustomerDetailModel model) {
        holder.tvNom.setText(model.getNom());
        holder.tvpreno.setText(model.getPrenom());
    }



    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults = new FilterResults();

                if(charSequence == null | charSequence.length() == 0){
                    filterResults.count = getUserModelListFiltered.size();
                    filterResults.values = getUserModelListFiltered;

                }else{
                    String searchChr = charSequence.toString().toLowerCase();

                    List<CustomerDetailModel> resultData = new ArrayList<>();

                    for(CustomerDetailModel userModel: getUserModelListFiltered){
                        if(userModel.getNom().toLowerCase().contains(searchChr)){
                            resultData.add(userModel);
                        }
                    }
                    filterResults.count = resultData.size();
                    filterResults.values = resultData;

                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                userModelList = (List<CustomerDetailModel>) filterResults.values;
                notifyDataSetChanged();

            }
        };
        return filter;
    }





    @NonNull
    @Override
    public CustomerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.lastname_list,parent,false);
        return new CustomerHolder(v);
    }

    @Override
    public void onError(FirebaseFirestoreException e) {
        Log.e("error", e.getMessage());
    }



    }


