package com.borakay.tennispro;

public class CustomerDetailModel {

    String nom,prenom,email,phone,age,club;


    public CustomerDetailModel(){

    }

    public CustomerDetailModel(String nom, String prenom, String email, String phone,String age,String club) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.phone = phone;
        this.age=age;
        this.club=club;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getEmail() {
        return email;
    }


    public String getPhone() {
        return phone;
    }

    public String getAge() {
        return age;
    }

    public String getClub() {
        return club;
    }


}
