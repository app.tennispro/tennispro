package com.borakay.tennispro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

public class SplashScreenActivity extends AppCompatActivity {
    final int SPLASH_DISPLAY_LENGTH = 10000;
    public ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        imageView = (ImageView)findViewById(R.id.ivtennispro);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                startSecondActivity();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }


    private void startSecondActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                SplashScreenActivity.this, imageView, ViewCompat.getTransitionName(imageView));
        startActivity(intent, options.toBundle());



    }
}
