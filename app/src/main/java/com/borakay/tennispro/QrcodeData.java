package com.borakay.tennispro;

public class QrcodeData {
    private String active_status,active_date,inactive_date;

    public QrcodeData()
    {

    }

    public QrcodeData(String active_status, String active_date, String inactive_date) {

        this.active_status = active_status;
        this.active_date = active_date;
        this.inactive_date = inactive_date;
    }


    public String getActive_status() {
        return active_status;
    }

    public String getActive_date() {
        return active_date;
    }

    public String getInactive_date() {
        return inactive_date;
    }
}
