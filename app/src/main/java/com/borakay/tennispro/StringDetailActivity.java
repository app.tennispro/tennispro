package com.borakay.tennispro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class StringDetailActivity extends AppCompatActivity {
Spinner spconditionnment,spmarque,spmodel,spLONGUEUR,spPoses,spPoses_restantes,spJauge,spcolor;
String conditionnment,string_marque,modele,LONGUEUR,Poses,Poses_restantes,Jauge,Tension,color,value,nom,prenom,email,mobile,club,age,familynom,familyprenom,familyemail,relation1,marque,gamme1,model,poids,tailledemanche,annee,etat,image,radioChosen;
Intent intent;
Button suiantbutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_string_detail);
        //transfer the data on activity two another
        value =getIntent().getStringExtra("value");
        nom =getIntent().getStringExtra("nom");
        prenom =getIntent().getStringExtra("prenom");
        email =getIntent().getStringExtra("email");
        mobile =getIntent().getStringExtra("mobile");
        age =getIntent().getStringExtra("age");
        club =getIntent().getStringExtra("club");
        familynom =getIntent().getStringExtra("familynom");
        familyprenom =getIntent().getStringExtra("familyprenom");
        familyemail =getIntent().getStringExtra("familyemail");
        relation1 =getIntent().getStringExtra("relation");
        marque=getIntent().getStringExtra("marque");
        gamme1=getIntent().getStringExtra("gamme1");
        model=getIntent().getStringExtra("model");
        poids=getIntent().getStringExtra("poids");
        tailledemanche=getIntent().getStringExtra("tailledemanche");
        annee=getIntent().getStringExtra("annee");
        etat=getIntent().getStringExtra("etat");
        radioChosen=getIntent().getStringExtra("radioChosen");
        image=getIntent().getStringExtra("image");

        init();


    }

    private void init() {
        //Spinner coding
        spconditionnment=(Spinner)findViewById(R.id.spconditionnement);
        spmarque=(Spinner)findViewById(R.id.spbrand);
        spmodel=(Spinner)findViewById(R.id.spmodel);
        spLONGUEUR=(Spinner)findViewById(R.id.spLONGUEUR);
        spPoses=(Spinner)findViewById(R.id.spPoses);
        spPoses_restantes=(Spinner)findViewById(R.id.spPoses_restantes);
        spJauge=(Spinner)findViewById(R.id.spJauge);

        //Adding data in Spinner

        List<String> conditionnment = new ArrayList<>();
       conditionnment.add("Garnitures");
       conditionnment.add("Bobines");


        ArrayAdapter<String> conditionnement = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, conditionnment);

        // Drop down layout style - list view with radio button
        conditionnement.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spconditionnment.setAdapter(conditionnement);

        List<String> string_brand = new ArrayList<>();
        string_brand.add("Babolat");
        string_brand.add("Dunlop");
        string_brand.add("Gamma");
        string_brand.add("Gosen");
        string_brand.add("Head");
        string_brand.add("Kirschbaum");
        string_brand.add("Luxilon");
        string_brand.add("MAILLOT SAVAREZ");
        string_brand.add("MSV");
        string_brand.add("Pacific");
        string_brand.add("Polyfibre");
        string_brand.add("Polystar");
        string_brand.add("Prince");
        string_brand.add("Robin Soderling");
        string_brand.add("Signum");
        string_brand.add("Solinco");
        string_brand.add("Tecnifibre");
        string_brand.add("TennisPro");
        string_brand.add("Toalson");
        string_brand.add("Volkl");
        string_brand.add("West Gut");
        string_brand.add("Wilson");
        string_brand.add("Xfactor");
        string_brand.add("Yonex");
        string_brand.add("Autre");

        ArrayAdapter<String> brand = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, string_brand);

        // Drop down layout style - list view with radio button
        brand.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spmarque.setAdapter(brand);








        //Button
        suiantbutton=(Button)findViewById(R.id.suivant);
        suiantbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               intent =new Intent(StringDetailActivity.this,DashboardActivity.class);
                intent.putExtra("value", value);
                intent.putExtra("nom", nom);
                intent.putExtra("prenom", prenom);
                intent.putExtra("email", email);
                intent.putExtra("mobile", mobile);
                intent.putExtra("club", club);
                intent.putExtra("age", age);
                intent.putExtra("familynom", familynom);
                intent.putExtra("familyprenom", familyprenom);
                intent.putExtra("familyemail", familyemail);
                intent.putExtra("relation", relation1);
                intent.putExtra("marque", marque);
                intent.putExtra("gamme1", gamme1);
                intent.putExtra("model", model);
                intent.putExtra("poids", poids);
                intent.putExtra("tailledemanche", tailledemanche);
                intent.putExtra("annee", annee);
                intent.putExtra("etat", etat);
                intent.putExtra("radioChosen", radioChosen);
                intent.putExtra("image", image);
                startActivity(intent);

            }
        });





    }
}
