package com.borakay.tennispro;

public class FamilyModel
{
    String nom,prenom,email,relation;


    public FamilyModel(){

    }

    public FamilyModel(String nom, String prenom, String email,String relation) {
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.relation=relation;

    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getEmail() {
        return email;
    }

    public String getRelation() {
        return relation;
    }
}
