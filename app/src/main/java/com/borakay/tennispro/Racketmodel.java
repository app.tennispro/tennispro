package com.borakay.tennispro;

public class Racketmodel {

    private String marque,gamme, model,tailledemanche,poids,acheteeEnMagasin,annee,etat;

    public Racketmodel(String marque, String gamme, String model, String tailledemanche, String poids, String acheteeenmagasin, String annee, String etat) {
       this.marque = marque;
        this.gamme = gamme;
        this.model = model;
        this.tailledemanche = tailledemanche;
        this.poids = poids;
        this.acheteeEnMagasin = acheteeenmagasin;
        this.annee = annee;
        this.etat = etat;
    }

    public Racketmodel() {
    }

    public String getMarque() {
        return marque;
    }

    public String getGamme() {
        return gamme;
    }

    public String getModel() {
        return model;
    }

    public String getTailledemanche() {
        return tailledemanche;
    }

    public String getPoids() {
        return poids;
    }

    public String getAcheteeEnMagasin() {
        return acheteeEnMagasin;
    }

    public String getAnnee() {
        return annee;
    }

    public String getEtat() {
        return etat;
    }
}
